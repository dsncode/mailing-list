package com.dsncode.mail.list.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;

import com.dsncode.mail.list.beans.Customer;
import com.dsncode.mail.list.beans.value.CustomerId;
import com.dsncode.mail.list.dao.CustomerDao;
import com.dsncode.mail.list.exception.CustomerException;
import com.dsncode.mail.list.exception.CustomerNotFoundException;
import com.dsncode.mail.list.exception.DBException;
import com.dsncode.mail.list.util.CustomerUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class CustomerDaoImpl implements CustomerDao{
	private static final ObjectMapper om = new ObjectMapper();
	private static ObjectNode db;
	
	private static CustomerDaoImpl singleton;
	
	static
	{
		try{
			//load initial data.
			db = (ObjectNode) om.readTree(CustomerDao.class.getClassLoader().getResourceAsStream("customers.json"));
		}catch(Exception e1){System.out.println("error when loading base db"); db=om.createObjectNode();}
		
//		try{
//			//load persisted data.
//			db = (ObjectNode) om.readTree(new File("db.json"));
//		}catch(Exception e){
//			try{
//				//load initial data.
//				db = (ObjectNode) om.readTree(CustomerDao.class.getClassLoader().getResourceAsStream("customers.json"));
//			}catch(Exception e1){System.out.println("error when loading base db"); db=om.createObjectNode();}
//		}
	}
	
	public static synchronized CustomerDao getInstance()
	{
		if(singleton==null)
			singleton = new CustomerDaoImpl();
		
		return singleton;
	}

	public Customer findCustomer(CustomerId id) throws CustomerException	
	{
		JsonNode node = db.path("customers").path(id.getId());
		if(node.isNull() || node.isMissingNode())
			throw new CustomerNotFoundException("Customer does not exist");
	
		return CustomerUtil.fromJsonNode(id, node);
	}
	
	private synchronized void saveDB() throws DBException 
	{
			try {
				File path = new File("db.json");
				om.writeValue(path, db);
				System.out.println("DB Saved to "+path.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
				throw new DBException("error when saving db "+e);
			}
	}
	
	public void persist(Customer customer) throws DBException
	{
		((ObjectNode)db.path("customers")).set(customer.getCustomerId().getId(), CustomerUtil.toJsonNode(customer));
//		System.out.println("changes on customer "+customer.getCustomerId().getId()+" were persisted");
		saveDB();
	}
	
	public void remove(Customer customer) throws DBException
	{
		((ObjectNode)db.path("customers")).remove(customer.getCustomerId().getId());
//		System.out.println("customer "+customer.getCustomerId().getId()+" was removed");
		saveDB();
	}
	
	public Set<Customer> getCustomers()
	{
		Iterator<Entry<String,JsonNode>> it = db.path("customers").fields();
		Set<Customer> customers = new HashSet<Customer>();
		while(it.hasNext())
		{
			Entry<String,JsonNode> node = it.next();
			try{
				customers.add(CustomerUtil.fromJsonNode(CustomerId.build(node.getKey()), node.getValue()));
			}catch(Exception e)
			{
				System.out.println("corrupted data. ignored");
			}
		}
		return customers;
	}
	
	public void main(String args[]) throws DBException, CustomerException
	{
		CustomerDao cdao = new CustomerDaoImpl();
		Customer daniel = cdao.findCustomer(CustomerId.build("IDAAA"));
		cdao.remove(daniel);
	}
}

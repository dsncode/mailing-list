package com.dsncode.mail.list.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.dao.CustomerDao;
import com.dsncode.mail.list.dao.EmailListDao;
import com.dsncode.mail.list.exception.DBException;
import com.dsncode.mail.list.exception.EmailNotSubscrivedException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class EmailListDaoImpl implements EmailListDao{

	private static final ObjectMapper om = new ObjectMapper();
	private static ObjectNode db;
	private static String MAILING_LIST = "mailingList";
	
	private static EmailListDaoImpl singleton;
	
	static
	{
		try{
			//load initial data.
			db = (ObjectNode) om.readTree(CustomerDao.class.getClassLoader().getResourceAsStream("customers.json"));
		}catch(Exception e1){System.out.println("error when loading base db"); db=om.createObjectNode();}
	}
	
	public static synchronized EmailListDaoImpl getInstance()
	{
		if(singleton==null)
			singleton = new EmailListDaoImpl();
		return singleton;
	}
	
	private synchronized void saveDB() throws DBException 
	{
			try {
				File path = new File("db.json");
				om.writeValue(path, db);
				System.out.println("DB Saved to "+path.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
				throw new DBException("error when saving db "+e);
			}
	}
	
	public void subscriveToMailingList(Email email) throws DBException
	{
		((ObjectNode)db.path(MAILING_LIST)).put(email.getValue(), true);
		saveDB();
	}
	
	public void unSubscriveFromMaillingList(Email email) throws EmailNotSubscrivedException, DBException{
		JsonNode ref = db.path(MAILING_LIST).path(email.getValue());
		if(ref.isNull() || ref.isMissingNode() || ref.asBoolean()==false)
			throw new EmailNotSubscrivedException("email "+email.getValue()+" is not subscrived");
		else
			((ObjectNode)db.path(MAILING_LIST)).put(email.getValue(), false);
		saveDB();
	}
	
	
	
	@Override
	public Set<Email> getSubscribedList() {
		Iterator<Entry<String,JsonNode>>it = db.path(MAILING_LIST).fields();
		Set<Email> subscrivedList = new HashSet<Email>();
		while(it.hasNext())
		{
			Entry<String,JsonNode> entry = it.next();
			try {
				if(entry.getValue().asBoolean())
					subscrivedList.add(Email.build(entry.getKey()));
			}
			catch(Exception e){
				System.out.println("corrupted email data "+entry.getKey());
			}
		}
		return subscrivedList;
	}

	@Override
	public boolean isSubscrived(Email email) throws DBException {
		JsonNode ref = db.path(MAILING_LIST).path(email.getValue());
			if(ref.isMissingNode() || ref.isNull() || ref.asBoolean()==false)
				return false;
			else 
				return true;
			
	}
	
}

package com.dsncode.mail.list.util;

import com.dsncode.mail.list.beans.Customer;
import com.dsncode.mail.list.beans.value.CustomerId;
import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.beans.value.Password;
import com.dsncode.mail.list.exception.InvalidEmailException;
import com.dsncode.mail.list.exception.InvalidPasswordException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class CustomerUtil {

	private static ObjectMapper om = new ObjectMapper();
	public static ObjectNode toJsonNode(Customer customer)
	{
		return om.createObjectNode()
				.put("name", customer.getName())
				.put("password", customer.getPassword().getValue())
				.put("email", customer.getEmail().getValue())
				.put("subscribed", customer.isSubscrived());
	}
	
	public static  Customer fromJsonNode(CustomerId id, JsonNode node) throws InvalidEmailException, InvalidPasswordException
	{
		return new Customer(id,
				node.path("name").asText(),
				Email.build(node.path("email").asText()), 
				Password.build(node.path("password").asText()),
				node.path("subscribed").asBoolean());
	}
	
	
}

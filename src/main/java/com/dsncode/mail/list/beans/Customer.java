package com.dsncode.mail.list.beans;

import com.dsncode.mail.list.beans.value.CustomerId;
import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.beans.value.Password;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class Customer {

	private final CustomerId userid;
	private final Email email;
	private final String name;
	private final Password password;
	private final boolean subscribed;
	public Customer(CustomerId userid, String name, Email email, Password password,boolean subscrived)
	{
		this.name=name;
		this.userid=userid;
		this.email=email;
		this.subscribed=subscrived;
		this.password=password;
	}
	
	public CustomerId getCustomerId() {
		return userid;
	}
	
	public Email getEmail()
	{
		return email;
	}
	
	public String getName() {
		return name;
	}
	
	public Password getPassword()
	{
		return password;
	}
	
	public boolean isSubscrived()
	{
		return subscribed;
	}
	
}

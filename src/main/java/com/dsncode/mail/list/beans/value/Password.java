package com.dsncode.mail.list.beans.value;

import java.util.regex.Pattern;

import com.dsncode.mail.list.exception.InvalidEmailException;
import com.dsncode.mail.list.exception.InvalidPasswordException;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public final class Password {
	
	private final String password;
	private static Pattern regex = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$");
	
	public static Password build(String pass) throws InvalidPasswordException
	{
		if(!regex.matcher(pass).matches())
			throw new InvalidPasswordException("invalid password "+pass);
		return new Password(pass);
	}
	
	private Password(String password)
	{
		this.password=password;
	}
	
	public boolean equals(Object value)
	{
		if(password.contentEquals(value.toString()))
			return true;
		else
			return false;
	}

	public String getValue()
	{
		return password;
	}
	
	public static void main(String args[]) throws InvalidPasswordException
	{
		Password.build("123@abcABC");
	}
	
}

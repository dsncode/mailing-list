package com.dsncode.mail.list.beans.value;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public final class CustomerId {
	
	private final String id;
	
	
	public static CustomerId build(String id)
	{
		return new CustomerId(id);
	}
	
	private CustomerId(String id)
	{
		this.id=id;
	}
	
	public boolean equals(Object value)
	{
		if(id.contentEquals(value.toString()))
			return true;
		else
			return false;
	}

	@Override
	public int hashCode()
	{
		return this.id.hashCode();
	}
	
	public String getId()
	{
		return id;
	}
	
}

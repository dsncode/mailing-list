package com.dsncode.mail.list.exception;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class CustomerException extends Exception{

	private static final long serialVersionUID = -684762280394363615L;

	public CustomerException(String msg)
	{
		super(msg);
	}
	
}

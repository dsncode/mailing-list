package com.dsncode.mail.list.exception;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class InvalidPasswordException extends CustomerException{

	private static final long serialVersionUID = -913847471539757966L;

	public InvalidPasswordException(String msg) {
		super(msg);
	}

}

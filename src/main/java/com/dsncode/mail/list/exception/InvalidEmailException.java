package com.dsncode.mail.list.exception;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class InvalidEmailException extends CustomerException{

	private static final long serialVersionUID = 945018704295863768L;

	public InvalidEmailException(String msg)
	{
		super(msg);
	}
	
}

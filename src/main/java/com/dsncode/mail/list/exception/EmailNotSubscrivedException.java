package com.dsncode.mail.list.exception;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class EmailNotSubscrivedException extends Exception{

	private static final long serialVersionUID = 5253098517293803485L;

	public EmailNotSubscrivedException(String msg)
	{
		super(msg);
	}
	
}

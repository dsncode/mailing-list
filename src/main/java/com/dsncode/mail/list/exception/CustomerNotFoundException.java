package com.dsncode.mail.list.exception;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class CustomerNotFoundException extends CustomerException{

	private static final long serialVersionUID = -8678894005344416581L;

	public CustomerNotFoundException(String msg)
	{
		super(msg);
	}
	
}

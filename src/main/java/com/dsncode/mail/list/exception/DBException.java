package com.dsncode.mail.list.exception;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class DBException extends Exception{

	private static final long serialVersionUID = 3339560295957923075L;

	public DBException(String msg)
	{
		super(msg);
	}
	
}

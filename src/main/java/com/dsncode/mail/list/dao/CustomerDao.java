package com.dsncode.mail.list.dao;

import java.util.Set;

import com.dsncode.mail.list.beans.Customer;
import com.dsncode.mail.list.beans.value.CustomerId;
import com.dsncode.mail.list.exception.CustomerException;
import com.dsncode.mail.list.exception.DBException;

/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public interface CustomerDao { 
	
	public Customer findCustomer(CustomerId id) throws CustomerException;
	public void persist(Customer customer) throws DBException;
	public void remove(Customer customer) throws DBException;
	public Set<Customer> getCustomers();
	
}

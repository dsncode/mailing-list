package com.dsncode.mail.list.dao;

import java.util.Set;

import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.exception.DBException;
import com.dsncode.mail.list.exception.EmailNotSubscrivedException;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public interface EmailListDao {

	public void subscriveToMailingList(Email email) throws DBException;
	public void unSubscriveFromMaillingList(Email email) throws EmailNotSubscrivedException, DBException;
	public Set<Email> getSubscribedList();
	public boolean isSubscrived(Email email) throws DBException;
	
}

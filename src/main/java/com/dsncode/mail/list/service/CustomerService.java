package com.dsncode.mail.list.service;

import com.dsncode.mail.list.beans.Customer;
import com.dsncode.mail.list.beans.value.CustomerId;
import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.beans.value.Password;
import com.dsncode.mail.list.dao.CustomerDao;
import com.dsncode.mail.list.exception.CustomerException;
import com.dsncode.mail.list.exception.DBException;
import com.dsncode.mail.list.exception.EmailNotSubscrivedException;
import com.dsncode.mail.list.impl.CustomerDaoImpl;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public interface CustomerService {

	public Customer findCustomer(CustomerId id) throws CustomerException;
	
	public void registerCustomer(Customer customer) throws DBException;
	public void removeCustomer(Customer customer) throws DBException;
	public Customer updatePassword(Customer customer, Password newPassword) throws DBException;
	
	public Customer updateEmail(Customer customer, Email newEmail) throws DBException;
	
	public void unsubscriveFromMailingList(Customer customer) throws DBException, EmailNotSubscrivedException;
	
}

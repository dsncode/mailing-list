package com.dsncode.mail.list.service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.dsncode.mail.list.beans.Customer;
import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.dao.CustomerDao;
import com.dsncode.mail.list.exception.DBException;
import com.dsncode.mail.list.exception.EmailNotSubscrivedException;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public interface EmailSubscriptionService {

	
	public Set<Email> getEmailSubscrivedList();
	
	public void sendEmailToAllSubscrivedList(String content);
	
	public void sendEmail(Email email,String content);
	
	public boolean isEmailSubscribed(Email email) throws DBException;
	public void unsubscriveFromEmailList(Email email) throws DBException, EmailNotSubscrivedException;
	public void subscriveToEmailList(Email email) throws DBException, EmailNotSubscrivedException;
		
}

package com.dsncode.mail.list.service.impl;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.dsncode.mail.list.beans.Customer;
import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.dao.CustomerDao;
import com.dsncode.mail.list.dao.EmailListDao;
import com.dsncode.mail.list.exception.DBException;
import com.dsncode.mail.list.exception.EmailNotSubscrivedException;
import com.dsncode.mail.list.service.EmailSubscriptionService;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class EmailSubscriptionServiceImpl implements EmailSubscriptionService{

	private EmailListDao emailListDao;
	
	public static EmailSubscriptionServiceImpl build(EmailListDao emailListDao)
	{
		return new EmailSubscriptionServiceImpl(emailListDao);
	}
	
	private EmailSubscriptionServiceImpl(EmailListDao emailListDao){
		this.emailListDao=emailListDao;
	}
	
	public Set<Email> getEmailSubscrivedList()
	{
		return emailListDao.getSubscribedList().stream().collect(Collectors.toSet());
	}
	
	public void sendEmailToAllSubscrivedList(String content)
	{
		getEmailSubscrivedList().stream().forEach(mail->sendEmail(mail,content));
	}
	
	public void sendEmail(Email email,String content)
	{
		//sends an email...
		System.out.println("email sent to "+email.getValue()+"...");
	}
	
	public boolean isEmailSubscribed(Email email) throws DBException
	{
		return emailListDao.isSubscrived(email);
	}
	
	public void unsubscriveFromEmailList(Email email) throws DBException, EmailNotSubscrivedException{
		
		emailListDao.unSubscriveFromMaillingList(email);
	}	
	
	public void subscriveToEmailList(Email email) throws DBException{
		
		
	}
	
		
}

package com.dsncode.mail.list.service.impl;

import com.dsncode.mail.list.beans.Customer;
import com.dsncode.mail.list.beans.value.CustomerId;
import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.beans.value.Password;
import com.dsncode.mail.list.dao.CustomerDao;
import com.dsncode.mail.list.exception.CustomerException;
import com.dsncode.mail.list.exception.DBException;
import com.dsncode.mail.list.exception.EmailNotSubscrivedException;
import com.dsncode.mail.list.impl.CustomerDaoImpl;
import com.dsncode.mail.list.service.CustomerService;
import com.dsncode.mail.list.service.EmailSubscriptionService;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class CustomerServiceImpl implements CustomerService{

	private CustomerDao customerDao;  
	private EmailSubscriptionService emailService; 
		
	private CustomerServiceImpl(CustomerDao customerDao, EmailSubscriptionService emailService)
	{
		this.customerDao = customerDao;
		this.emailService = emailService;
	}
	
	public static CustomerServiceImpl build(CustomerDao customerDao,EmailSubscriptionService emailService)
	{
		return new CustomerServiceImpl(customerDao,emailService);
	}
	
	public Customer findCustomer(CustomerId id) throws CustomerException
	{
		return customerDao.findCustomer(id);
	}
	
	public void registerCustomer(Customer customer) throws DBException
	{
		customerDao.persist(customer);
	}
	
	public void removeCustomer(Customer customer) throws DBException
	{
		customerDao.remove(customer);
	}
	
	public Customer updatePassword(Customer customer, Password newPassword) throws DBException
	{
		//I manage to create a new object. to avoid side effects issues.
		Customer new_state = new Customer(customer.getCustomerId(),customer.getName(),customer.getEmail(),newPassword,customer.isSubscrived());
		customerDao.persist(new_state);
		return new_state;
	}
	
	public Customer updateEmail(Customer customer, Email newEmail) throws DBException
	{
		//I manage to create a new object. to avoid side effects issues.
		Customer new_state = new Customer(customer.getCustomerId(),customer.getName(),newEmail,customer.getPassword(),customer.isSubscrived());
		customerDao.persist(new_state);
		return new_state;
	}
	
	public void unsubscriveFromMailingList(Customer customer) throws DBException, EmailNotSubscrivedException
	{
		emailService.unsubscriveFromEmailList(customer.getEmail());
	}
	
}

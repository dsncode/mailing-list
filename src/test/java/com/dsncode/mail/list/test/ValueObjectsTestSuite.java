package com.dsncode.mail.list.test;

import org.junit.Test;

import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.beans.value.Password;
import com.dsncode.mail.list.exception.InvalidEmailException;
import com.dsncode.mail.list.exception.InvalidPasswordException;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class ValueObjectsTestSuite {

	@Test(expected = InvalidPasswordException.class)
	public void invalidPasswordValidation() throws InvalidPasswordException
	{
		Password.build("123");
	}
	
	@Test
	public void validPassword() throws InvalidPasswordException
	{
		Password.build("123@abcABC");
	}

	@Test(expected = InvalidEmailException.class)
	public void invalidEmail() throws InvalidEmailException
	{
		Email.build("d.silva@live");
	}
	
	
}

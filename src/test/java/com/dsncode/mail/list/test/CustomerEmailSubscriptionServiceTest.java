package com.dsncode.mail.list.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.exception.DBException;
import com.dsncode.mail.list.exception.EmailNotSubscrivedException;
import com.dsncode.mail.list.exception.InvalidEmailException;
import com.dsncode.mail.list.impl.EmailListDaoImpl;
import com.dsncode.mail.list.service.EmailSubscriptionService;
import com.dsncode.mail.list.service.impl.EmailSubscriptionServiceImpl;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class CustomerEmailSubscriptionServiceTest {

	EmailSubscriptionService customerEmailService = EmailSubscriptionServiceImpl.build(EmailListDaoImpl.getInstance());
	
	@Test
	public void unsuscribe() throws InvalidEmailException, DBException, EmailNotSubscrivedException
	{
		Email email = Email.build("min@email.com");
		customerEmailService.unsubscriveFromEmailList(email);
		assertEquals(customerEmailService.isEmailSubscribed(email),false);
	}
	
	
	
}

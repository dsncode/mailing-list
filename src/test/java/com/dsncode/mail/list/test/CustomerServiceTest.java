package com.dsncode.mail.list.test;

import org.junit.Test;

import com.dsncode.mail.list.beans.Customer;
import com.dsncode.mail.list.beans.value.CustomerId;
import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.beans.value.Password;
import com.dsncode.mail.list.dao.CustomerDao;
import com.dsncode.mail.list.dao.EmailListDao;
import com.dsncode.mail.list.exception.CustomerException;
import com.dsncode.mail.list.exception.CustomerNotFoundException;
import com.dsncode.mail.list.exception.DBException;
import com.dsncode.mail.list.impl.CustomerDaoImpl;
import com.dsncode.mail.list.impl.EmailListDaoImpl;
import com.dsncode.mail.list.service.CustomerService;
import com.dsncode.mail.list.service.impl.CustomerServiceImpl;
import com.dsncode.mail.list.service.impl.EmailSubscriptionServiceImpl;
/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class CustomerServiceTest {

    CustomerDao customerDao = CustomerDaoImpl.getInstance();
    EmailListDao emailListDao = EmailListDaoImpl.getInstance();
	CustomerService customerService = CustomerServiceImpl.build(customerDao,EmailSubscriptionServiceImpl.build(emailListDao));
	
	
	@Test
	public void findUser() throws CustomerException
	{
		customerService.findCustomer(CustomerId.build("IDAAA"));
	}
	
	@Test(expected = CustomerNotFoundException.class) 
	public void removeUser() throws CustomerException, DBException
	{
		Customer jenny = customerService.findCustomer(CustomerId.build("IDCCC"));
		customerService.removeCustomer(jenny);
		customerService.findCustomer(CustomerId.build("IDCCC"));
	}
	
	@Test
	public void registerUser() throws DBException, CustomerException
	{
		Customer jack = new Customer(CustomerId.build("AAA"), "Jack", Email.build("jack@live.cl"), Password.build("123@abcABC"),true);
		customerService.registerCustomer(jack);
		customerService.findCustomer(CustomerId.build("AAA"));
	}
	
}

package com.dsncode.mail.list.test;

import org.junit.Test;

import com.dsncode.mail.list.beans.value.Email;
import com.dsncode.mail.list.dao.EmailListDao;
import com.dsncode.mail.list.exception.DBException;
import com.dsncode.mail.list.exception.EmailNotSubscrivedException;
import com.dsncode.mail.list.exception.InvalidEmailException;
import com.dsncode.mail.list.impl.EmailListDaoImpl;

import static org.junit.Assert.assertEquals;

/**
 * @author Daniel Silva
 * @see linkedin: http://bit.ly/dsnlinkedin
 * other projects: http://bit.ly/dsncode-gitlab
 */
public class EmailDaoTest {

	EmailListDao emailDao = EmailListDaoImpl.getInstance();
	
	@Test
	public void emailSubscrived() throws InvalidEmailException, DBException
	{
		boolean subscrived = emailDao.isSubscrived(Email.build("d.silva@live.cl"));
		assertEquals(subscrived,true);
	}
	@Test
	public void subscriveMail() throws DBException, InvalidEmailException
	{
		Email email = Email.build("dummy@live.cl");
		emailDao.subscriveToMailingList(email);
		assertEquals(emailDao.isSubscrived(email),true);
	}
	
	@Test
	public void subscriveAndUnsubscrive() throws InvalidEmailException, DBException, EmailNotSubscrivedException
	{
		Email email = Email.build("dummy@live.cl");
		emailDao.subscriveToMailingList(email);
		assertEquals(emailDao.isSubscrived(email),true);
		emailDao.unSubscriveFromMaillingList(email);
		assertEquals(emailDao.isSubscrived(email),false);
	}
	
}
